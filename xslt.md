---
Contributeur.ice.s: Guillaume Porte
Date de création: 2023
Modifications: 2024-06
Titre: XPATH
Licence: CC BY-SA
Description: support pour une formation en présentiel à XSLT - niveau débutant.
---

# XSLT 

"XSLT (eXtensible Stylesheet Language Transformations), défini au sein de la recommandation XSL du W3C, est un langage de transformation XML de type fonctionnel. Il permet notamment de transformer un document XML dans un autre format, tel PDF ou encore HTML pour être affiché comme une page web." ([wikipedia](https://fr.wikipedia.org/wiki/Extensible_Stylesheet_Language_Transformations))

- un fichier XSL s'écrit en XML
- l'extension est `.xsl`

![principe](images/xslt.svg){width=50%}

Avant de commencer, on ouvre le fichier `courrier-n74.xml` dans le navigateur.

"Aucune information de style ne semble associée à ce fichier XML. L’arbre du document est affiché ci-dessous."


## Création de la feuille XSL

Créer un fichier `courrier.xsl` avec ce contenu : 

```
<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="tei xsl"
    version="2.0">

    <xsl:output method="html"/>

 	<!-- instructions pour la transformation -->

</xsl:stylesheet>
```

## Faire une transformation XSL avec son navigateur

### Configurer le navigateur

Pour tester les transformations XSLT sur son navigateur (d'après [ce site](https://cours.ebsi.umontreal.ca/INU3011/H2023/proto/exerc-XSLT-1/)) :
- installer [Firefox](https://www.mozilla.org/fr/firefox/new/)
- dans la barre d'URL, taper `about:config`
- cliquer sur `Accepter le risque et poursuivre` (ça craint rien)
- dans la barre de recherche qui s'affiche, taper `security.fileuri.strict_origin_policy`
- dans le résultat qui s'affiche :
  - si `false` est affiché, il n'y a rien à faire 
  - si `true` est affiché, double-cliquer dessus pour `false` s'affiche
- c'est bon

### Associer la feuille de style au document XML

En haut du document XML à transformer, juste après la déclaration XML (`<?xml version="1.0" ?>`), mettre cette instruction :

`<?xml-stylesheet href="MA-XSL.xsl" type="text/xsl"?>`

Il faudra juste remplacer `MA-XSL.xsl` par le nom du fichier XSL qu'on veut utiliser (dans le cadre de cet exercice : `courrier-n74.xsl`)


## Alternative : faire une transformation XSL dans BaseX avec XQuery

! attention !
Pour cette technique, il faut :
- que le fichier `.xsl` contienne l'instruction `<xsl:output method="xml"/>` après la déclaration `<xsl:stylesheet>`
- mettre dans `basex/lib/custom` la librairie `saxon9he.jar` qui se trouve (ici)

- installer BaseX
- ouvrir Basex GUI (dans basex/bin)
- dans la barre de menu suivante, sélectionnez uniquement le crayon et le `<x>`
![barre de menu BaseX](barrebasexgui.png)
- dans la barre de menu suivante, cliquez sur le logo page (ou `Editor > New` ou `ctrl+t` ou `cmd+t`)
![barre de menu BaseX - editeur XQuery](barrebasexgui2.png)
- dans l'éditeur qui vient de s'ouvrir, insérer ce code :
```
(: on précise qu'on veut du HTML :)
declare option output:method 'html';
(: du HTML, dernière version :)
declare option output:html-version '5.0';
(: et du HTML avec un encodage UTTF-8 :)
declare option output:encoding 'UTF-8'; 

(: chemin absolu du dossier de travail sur votre ordinateur, par exemple C://Users/moi/Bureau/XSLT/ :)
let $chemin := "/Le/Chemin/Vers/Mon/Dossier"

(: nom de la feuille XSLT :)
let $FichierXSLT := doc($chemin || "courrier-n74.xsl") 
(: nom du fichier XML :)
let $FichierXML := doc($chemin || "courrier-n74.xml") 
(: opération de transformation XML vers HTML :)
let $FichierHTML := xslt:transform($FichierXML, $FichierXSLT) 

(: résultats :)
return 
(
  (: affichage du résultat dans BASEX :)
  $FichierHTML,
  (: enregistrement du résultat dans un fichier HTML :)
  file:write($chemin || "courrier-n74.html", $FichierHTML) 
)

```
- et remplacer le contenu de la varibale `$chemin` par le chemin vers le dossier dans lequel se trouvent les documents
- enregistrer le tout dans le dossier `basex/src` en cliquant le logo disquette ou `ctrl+s` ou `cmd+s` – on peut appeler le fichier `xslt.xq`
- cliquer sur le bouton lecture pour lancer la transformation
- le fichier `.html` est enregistré dans le dossier indiqué dans la variale `$chemin`

## Instructions de transformation

### Les templates

**Principe**

```
<xsl:template select="<!-- sélecteur XPATH] -->">
<!-- transformation choisie -->
</xsl:template>
 ```

 **Exemples**

 _note : à chaque fois qu'on ajoute une instruction au fichier XSL, on le sauvegarde et on recharge le fichier XML dans le navigateur pour voir les changements_

1. Dans le fichier `courrier.xsl`, insérer le code suivant en dessous du commentaire `<!-- instructions pour la transformation -->`

```
<xsl:template match="//tei:p">
    <p><xsl:apply-templates/></p>
</xsl:template>
```

2. Ajouter à la feuille XSL une nouvelle instruction en dessous de la précédente :

```
<xsl:template match="//tei:p">
    <p><xsl:apply-templates/></p>
</xsl:template>
```

3. Essayer de distinguer les trois niveaux de titre en tranformant :

- `<head type="mainTitle">` en `<h1>`
- `<head type="subTitle">`  en `<h2>`
- `<head type="articleTitle">` en `<h3>`

4. Nouveau test avec l'instruction suivante :

```
<xsl:template select="//tei:teiHeader">
</xsl:template>
```

5. Et en ajoutant celle-ci ?

```
<xsl:template select="/">
</xsl:template>
```

6. On modifie l'instruction précédente pour ajouter les balises html

```
<xsl:template select="/">
<html>
	<head>
	</head>
	<body>
	</body>
</html>
</xsl:template>
```

On a tout cassé...

7. On va ajouter `<xsl:apply-templates/>` entre les deux balises `<body></body>`

```
<xsl:template select="/">
<html>
	<head>
	</head>
	<body>
		<xsl:apply-templates/>
	</body>
</html>
</xsl:template>
```

8. Tant qu'à y être on peut ajouter la balise qui permet d'associer une feuille CSS et la balise `<title>` (entre les balises `<head></head>`)

```
<xsl:template select="/">
<html>
	<head>
		<link href="courrier.css" rel="stylesheet" type="text/css"/>
        <title>Courrier de Strasbourg - n° 74</title>
	</head>
	<body>
		<xsl:apply-templates/>
	</body>
</html>
</xsl:template>
```

9. Tester : 

- faire ressortir les noms de personnes en gras 
- mettre les noms de lieux entre parenthèses

10. Quoi d'autre ? 