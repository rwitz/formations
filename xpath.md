---
Contributeur.ice.s: Guillaume Porte
Date de création: 2023
Modifications: 2024-06
Titre: XPATH
Licence: CC BY-SA
Description: support pour une formation en présentiel à XSLT - niveau débutant.
---

# XPATH

"XPath est un langage de requête pour localiser une portion d'un document XML." ([wikipedia](https://fr.wikipedia.org/wiki/XPath#))

## L’arbre XML

Prenons ce document HTML :

```
<html>
  <head>
    <title>My page</title>
  </head>
  <body>
    <h2>Welcome to my <a href="#">page</a></h2>
    <p>This is the first paragraph.</p>
    <!-- this is the end -->
  </body>
</html>
```

qui en fait rebient à ça :

```
<html><head><title>My page</title></head><body><h2>Welcome to my <a href="#">page</a></h2><p>This is the first paragraph.</p><!-- this is the end --></body></html>
```

et qu'on peut représenter ainsi :

![arbre XML](https://cdn2.hubspot.net/hubfs/4367560/tree-7.png)

(exemple et image : https://www.zyte.com/blog/an-introduction-to-xpath-with-examples/)


Un autre exemple, le but est de dessiner l'arbre xml :

```
<xml>
  <div type="chapitre">
    <head>Chapitre 1</head>
    <p>blablabla</p>
    <div type="article">
        <head>Article 1</head>
        <p>lorem ipsum</p>
        <div>
            <date>2020</date>
            <p>dolor sit</p>
        </div>
        <div>
            <date>2026</date>
            <p>amet</p>
        </div>
    </div>
  </div>  
</xml>
```

## Syntaxe XPATH 

à (re)voir ici [https://www.w3schools.com/xml/xpath_intro.asp](https://www.w3schools.com/xml/xpath_intro.asp)


### Principes généraux

On commence en général l’instruction par un `/`, qui indique la racine du document.

`/` => 1 niveau en dessous (child)

- `/div` => tous les éléments `<div>` situés à la racine du document
- `/div/head` => tous les éléments `<head>` enfants d’un élément `<div>` situé à la racine du document

`//` => tous les niveaux en dessous (descendant)

- `//div` => n’importe quel élément `<div>` dans le document
- `//div//head` => tous les éléments `<head>` enfants ou descendants d’un élément `<div>` situé à n’importe quel endroit du document

`*` => n’importe quel élément, quel que soit son nom

`//div/*` => n’importe quel enfant d’un élément `<div>`

`@` => sélectionne un attribut

`//div/@type` => attribut "type" de l’élément `<div>`

`.` => dernier élément sélectionné

### parenté 

`child::` => 1 niveau en dessous (identique à `/`)

- `//div/child::head` = `/div/head`

`descendant::` => n’importe quel niveau en dessous (identique à `//`)

- `//div/descendant::head` = `/div//head`

`parent::` => 1 niveau au dessus du l’élément précédent

- `//head/parent::div` = le `<div>` qui est le parent d’un `<head>`
      
`ancestor::` => n’importe quel niveau au dessus de l’élément précédent

- `//head/ancestor::div` = tous les `<div>` dans lesquels est contenu un  `<head>`

`following::*` ou `preceding::*` => tous les éléments situés après (following) ou avant (preceding) l’élément précédent, quelque soit leur position dans le document

`following-sibling::*` ou `preceding-sibling::*`  => tous les éléments situés après ou avant l’élément précédent et qui ont le même parent


### prédicats

`//div/p[1]` => sélectionne le 1er élément `<p>` d’un élément `<div>`

`//div[@type]` => sélectionne les éléments `<div>` qui possèdent un attribut @type

`//div[@type="chapitre"]` => sélectionne les éléments `<div>` qui possèdent un attribut @type dont la valeur est "chapitre"

`//div[date]` => sélectionne les éléments `<div>` qui possèdent un ou des enfants `<date>`

`//div[date>2020]` => sélectionne les éléments `<div>` qui possèdent un ou des enfants `<date>` dont la valeur est supérieur à 2020

`//p[contains(.,"blablabla")]` => sélection les éléments `<p>` qui contiennent le texte `blablabla`


### opérateurs :

- `>` (supérieur)
- `>=` (supérieur ou égal)
- `<` (inférieur)
- `=<` (inférieur ou égal)
- `=` égal
- `!=` différent 
- `or` (ou => `date>2020` or `date<2023`)
- `and`  (et => `date>2020` and `date<2023`)

## Essayez !

1. allez sur http://xpather.com/

2. effacer le cotenu du bloc de gauche et copiez/collez-y le contenu du fichier qui est dans `ressources/bookstore.xml` ou à cette adresse : https://gitlab.huma-num.fr/estrades/formations/-/raw/main/ressources/bookstore.xml

_**note** => les XPATH sont à construire dans la barre du haut, les résultats (éléments sélectionnés) sont surlignés et sont aussi détaillés dans la colonne de droite_

3. on peut essayer quelques XPATH :

    - sélectionnez l’élément `<title>` de chaque élément `<book>`
    - sélectionnez l’élément `<title>` de chaque élément `<book>` dont l’attribut `@category` est `lifestyle`
    - sélectionnez les éléments `<book>` dont l’élément `<price>` est supérieur à 20
    - sélectionnez l’attribut `@lang` de l’élément `<title>` pour un `<book>` dont l’élément `<year>` est inférieur à 2020

