<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="tei xsl"
    version="2.0">

    <xsl:output method="html"/>
    
    <xsl:template match="//tei:p">
        <p><xsl:apply-templates/></p>
    </xsl:template>

    <xsl:template match="//tei:head[@type='mainTitle']">
        <h1><xsl:apply-templates/></h1>
    </xsl:template>

    <xsl:template match="//tei:head[@type='subTitle']">
        <h2><xsl:apply-templates/></h2>
    </xsl:template>

    <xsl:template match="//tei:head[@type='articleTitle']">
        <h3><xsl:apply-templates/></h3>
    </xsl:template>

    <xsl:template match="//tei:teiHeader">
	</xsl:template>

	<xsl:template match="/">
	<html>
		<head>
			<link href="courrier.css" rel="stylesheet" type="text/css"/>
	        <title>Courrier de Strasbourg - n° 74</title>
		</head>
		<body>
			<xsl:apply-templates/>
		</body>
	</html>
	</xsl:template>


</xsl:stylesheet>