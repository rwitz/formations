# Notes pour la formation

Regex pour extraire le texte des balises Unicode de la sortie Alto : `<TextLine .*\n.*\n.*\n.*\n *<Unicode>(.*)<\/Unicode>\n.*\n.*`

Pour matcher les chiffres romains : `[V|I|X]+\.`

## Pas-à-pas

- ouvrir N_74b.xml
- mettre deux teiHeader -> ça marche !
- comment restreindre ? Créer un fichier .rng.
- aller sur Roma et créer un fichier RNG pour tei_lite
- changer le nom du fichier rng en `rng_formation.rng`
- Mettre en début de document : `<?xml-model href="./rng_formation.rng" schematypens="http://relaxng.org/ns/structure/1.0"?>` 
- on ne peut plus mettre deux teiHeader !
- ajouter une balise persName et voir l'erreur
- chercher dans son fichier `rng_formation.rng` la chaîne de caractère suivante : `define name="tei_persName"` et constater qu'elle n'existe pas. Il va donc falloir l'ajouter !
- créer un autre fichier tei_lite.rng en ajoutant cette fois-ci la balise `<persName>`.
- extraire le morceau correspondant à la définition de l'élément `<persName>` et l'étudier.
- le coller dans notre fichier `rng_formation.rng`
- constater que cela ne fonctionne toujours pas -> ajouter l'élément ainsi dans la définition du modèle 

```
<define name="tei_model.nameLike.agent">
      <choice>
         <ref name="tei_name"/>
         <ref name="tei_persName"/>
      </choice>
   </define>
```

voir que cela correspond au nom du modèle donné par les guidelines, et faire de même pour les autres modèles.
- plus de problèmes avec persName !
- faire la même chose avec `<placeName>`

- regarder les attributs associés à `<persName` dans mon fichier rng (calendar en option)
- essayer de mettre l'attribut @pouet -> ne fonctionne pas, le changer par calendar. Ou mettre "pouet" dans le fichier rng. 

**Malheureusement on atteint les limites de sublime text...**

- on bascule sur Oxygen
- on veille à bien configurer le scénario de transformation
- on change le nom de l'attribut @calendar en un autre nom et également le type de data (date, string, URI...)
- et maintenant on va ajouter un attribut @PATATE à l'élément `<placeName>`
- autre manière de faire, ajouter l'attribut dans un groupe d'attributs
- exemple si je veux ajouter l'attribut @fleur à l'élément `<persName>`, il faut regarder les attributs déclarés au niveau de `define name="tei_persName"` voir qu'il y a le groupe `<ref name="tei_att.global.attributes"/>`
- aller chercher au niveau de la déclaration de ce groupe `define name="tei_att.global.attributes` et voir qu'il est composé de plusieurs attributs différents
- déclarer un nouvel attribut @fleur ainsi :

```
<define name="tei_att.global.attribute.fleur">
      <optional>
         <attribute name="fleur">
            <a:documentation xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0">donne le nom d'une fleur</a:documentation>
            <data type="string"/>
         </attribute>
      </optional>
   </define>
```
et déclarer ce nouvel attribut dans la liste d'attribut de `define name="tei_att.global.attributes` ainsi : `<ref name="tei_att.global.attribute.fleur"/>`. L'attribut @fleur fait maintenant parti des attributs autorisés par le groupe `tei_att.global.attributes` que l'on retrouve dans l'élément `<persName>`.

- pour ajouter un attribut @PATATE : 

```
<optional>
            <attribute name="PATATE">
               <a:documentation xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0">les féculents c'est important</a:documentation>
               <list>
                  <oneOrMore>
                     <data type="date"/>
                  </oneOrMore>
               </list>
            </attribute>
         </optional>
```

## schéma XML final

```
<?xml version="1.0" encoding="UTF-8"?>
<!--validation par un schéma rng -->
<?xml-model href="./rng_formation.rng" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
<teiHeader>
	<fileDesc>
		<titleStmt>
			<title>titre du fichier</title>
		</titleStmt>
		<publicationStmt>
  <p>Distributed as part of TEI P5</p>
 </publicationStmt>
		<sourceDesc>
			<p>No print source exists: this is an original digital text</p>
		</sourceDesc>
	</fileDesc>
	<profileDesc>
	</profileDesc>
</teiHeader>
<text>
	<body>
		<div>
			<head>Mon titre</head>
			<p>mon premier paragraphe dans lequel on trouve une <persName calendar="https://monurl.fr">personne</persName> et un <placeName PATATE="2024-06-24">lieu</placeName>.</p>
		</div>
	</body>
</text>
</TEI>
```
