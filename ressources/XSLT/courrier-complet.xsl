<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="tei xsl"
    version="2.0">

    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
          <head>
            <link href="courrier.css" rel="stylesheet" type="text/css"/>
            <title>Courrier de Strasbourg</title>
        </head>
        <body>
        	<header>
		      <a href="#" >
		        <img
		          class="logo"
		          src="tampon.jpg"
		          alt="Logo de mon site - Courrier de Strasbourg"
		        />
		      </a>
		    </header>
        	<main>
	        		<div><xsl:value-of select="//tei:fw"/></div>

				 	<div class="titles">
				 		<xsl:for-each select="//tei:head[@type='mainTitle']">
				 			<h1>
				 				<xsl:attribute name="lang">
				 					<xsl:value-of select="./@xml:lang"/>
				 				</xsl:attribute>
				 				<xsl:apply-templates/>
				 			</h1>
				 			<hr class="wavy" />
				 		</xsl:for-each>
				    </div>

				    <div id="top">
				        <div class="top-dates">
				         <xsl:for-each select="//tei:body/tei:p/tei:date">
					        <p><xsl:apply-templates/></p>
				      	</xsl:for-each>
				        </div>

				        <div class="encart" lang="de">
				          	<xsl:for-each select="//tei:head[@type='subTitle']">
				 				<xsl:apply-templates/>
				 			</xsl:for-each>
				        </div>
				    </div>

	            	<article>
	            		<section lang="fr">
	            			<xsl:for-each select="//tei:div[@xml:lang='fr']">
	            				<xsl:apply-templates/>
	            			</xsl:for-each>
	            		</section>
	            		<section lang="de">
	            			<xsl:for-each select="//tei:div[@xml:lang='de']">
	            				<xsl:apply-templates/>
	            			</xsl:for-each>
	            		</section>
	            	</article>

        	</main>
        </body>
        </html>
    </xsl:template>

    <xsl:template match="tei:teiHeader">
    </xsl:template>

    <xsl:template match="tei:head">
        <h3><xsl:apply-templates/></h3>
    </xsl:template>

    <xsl:template match="tei:head[not(@type='articleTitle')]">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:div">
        <div><xsl:apply-templates/></div>
    </xsl:template>

    <xsl:template match="tei:p">
        <p><xsl:apply-templates/></p>
    </xsl:template>

    <xsl:template match="tei:date">
        <time>
        	<xsl:attribute name="datetime">
        		<xsl:value-of select="./@when"/>
        	</xsl:attribute>
        	<xsl:apply-templates/>
       	</time>
    </xsl:template>

</xsl:stylesheet>