


---
Contributeur.ice.s: Elsa Van Koth et Marine Parra
Date de création: 2024-06-20
Titre: Texte des Slide formation TEI
Licence: CC BY-SA
Description: Pour résumer, ce sont nos notes ;) les suites de trois tirets indique un changement de slides
---

# Formation TEI
## 25 juin 2024 | Elsa & Marine

Qui dans la salle a déjà fait un peu de TEI ?

---
# Fichiers à ouvrir / à garder à portée de main
	- Les slides "Formation_TEI_2024_EVK-MP" (si nous passons à la suivante et que vous avez besoin d'un pas-à-pas, ce sera plus pratique pour vous ;)
	Vous avez la version pdf en mode présentation + le fichier md avec nos notes
	- La numérisation du numéro du Journal sur lequel nous travaillons
	- Le fichier txt avec la transcription de la source
	**Optionnel** 	
	Si vous voulez faire des exercices complémentaires (sur l'encodage d'éléments graphiques, de tables des matières par exemple, voir le dossier exercice complémentaire que nous n'aurons pas le temps de traiter ensemble en pas à pas).

---
# Liens à portée de main
	- Matériaux de la formation https://gitlab.huma-num.fr/estrades/formations/-/blob/main/ressources/TEI/N_74.txt?ref_type=heads
	- Guidelines https://tei-c.org/release/doc/tei-p5-doc/en/html/index.html
	- Roma : https://romabeta.tei-c.org
	**Optionnel**
	- TEI [Tool Box](http://teicat.huma-num.fr/index.php)
---

# Plan de séance
	- Configuration Sublim Text
	- Intro : situer la TEI dans la chaîne éditoriale
	- Point XML
	- Point TEI
	- Présentation des Guidelines 
	- Encodage structurel
	- Encodage sémantique par groupe
	*Pause*

	- Créer un index des lieux
	- Renseigner le TEI Header
	- Créer un schéma TEI 
	- (Visite commentée de quelques éditions numériques déjà disponibles à la consultation) Si on a du temps


---
	Configuration sublim Text
![](media/Capture d’écran 2024-06-17 à 13.32.55.png)

---

![](media/Capture d’écran 2024-06-17 à 13.34.08.png)


---
	Si l'installation est en cours, cela sera indiqué en bas à gauche de la fenêtre :

![](media/Capture d’écran 2024-06-17 à 13.34.26.png)


---
	Une fois terminé, le message suivant s'affiche :

![](media/Capture d’écran 2024-06-17 à 13.45.47.png)


---

	> **Une fois installé : quitter et rouvrir.**
	
	Vous pouvez ensuite récupérer le texte dans le dossier de [matériaux] (https://gitlab.huma-num.fr/estrades/formations/-/blob/main/ressources/TEI/N_74.txt?ref_type=heads) de la formation

---

	Quand vous ouvrez votre texte avec Sublime text, pensez à tout de suite l'enregistrer avec le titre de votre choix et **l'extension .xml** 
	Il faut aussi penser à le renommer, pour ne pas écraser le précédant.

![](media/Capture d’écran 2024-06-17 à 13.50.11.png)

---

# La TEI dans la chaîne de traitement
![](media/Schema_humanum_projetHN.png)


A l'échelle d'un projet de recherche, on entre dans l'étape du traitement, celle du milieu.
Ce schéma est celui qui convient pour tout type de projet, dans le cas de l'édition numérique, on peut l'adapter un peu éventuellement.

---

![](media/ChaineDeTraitement_MP_240620.jpg)


---

![](media/ChaineDeTraitement_AcEtapeRecherche_MP_240620.jpg)


---

#XML TEI
## Focus sur le XML
---
	Extension : .xml
	La TEI est basée sur le XML (eXtensible Markup Language)
	-> Utiliser pour structurer des données
	Langage lisible, interopérable, extensible.
	Tout un environnement compatible : XForms ; Xpointer ; RestXQ ; XSD ; etc
---

![](media/Balise_def.png)

	Exemple : 
		<name>Romain Gary</name>

Balisage : Le balisage d’une ressource permet de faciliter le traitement du document par des programmes informatiques. Le fonctionnement est le suivant : une balise (ou un marqueur) est placée sur chaque phénomène qu’on souhaite pouvoir ensuite exploiter. Les programmes informatiques dépendent de ces marqueurs, car à défaut de ce travail de préparation, un texte numérisé n’est rien d'autre qu'une séquence de bits indifférenciés.
---
# Quelques règles de base du XML
	Les noms de balises : 
	Le nom peut contenir des lettres, des chiffres ou d'autres caractères.
	Il ne doit pas commencer par un chiffre ou un signe de ponctuation.
	il ne doit pas non plus commencer par xml, Xml, XML...
	Aucun espace dans le nom d'une balise n'est toléré.
---

![](media/Regle_1.png)


---

![](media/Regle_2.png)


---


#XML TEI
## Focus sur la TEI

---
		<element attribut="valeur"></element>
	L'élément peut, si besoin, être qualifié par un attribut. 
	On ne peut pas inventer un élément, ni un attribut (enfin ... on peut, mais ce n'est plus de la TEI).
	La valeur de l'attribut est libre.

---

# Structure du fichier TEI
		<TEI xmlns=“http://www.tei-c.org/ns/1.0”>
		<teiHeader>…</teiHeader>
		<text>
		     <front>…</front>
		     <body>
		          <div>…</div>
		     </body>
		     <back>…</back>
		</text>
		</TEI>


---


	# Importer le texte dans votre éditeur XML
	## Plusieurs options
	1. Le texte est déjà transcrit
	Ne pas le copier coller depuis word (ou traitement de texte équivalent)
	-> passer par un éditeur de texte basique, enregistré en .txt / format UTF-8 (encodage dit « unicode » = codage de caractères informatiques qui permet de coder l’ensemble des caractères du « répertoire universel de caractères codés »).
	2. Le texte est dans un format particulier (alto s'il vient d'Escriptorium par exemple, dans ce cas, une étape supplémentaire à réaliser)

Ceux qui sont à l'aise, lancez-vous !
Les autres, on va visiter les guidelines :)
---

![](media/Guidelines-Table.png)


Ajd, nous n'aurons bien sûr pas le temps de regarder toutes les balises ou tous les cas de figure, mais ce qui compte, c'est plutôt de savoir où chercher les informations dont on a besoin en fonction des projets, et ensuite, on est autonome pour adapter les choses selon les besoins.

Équivalent d’un manuel d’utilisation disponible en ligne. 
Les recommandations existent en plusieurs langues (toutes les pages ne sont pas traduites il me semble). L’anglais est la langue par défaut, y compris des balises et attributs. 
À partir d’une table des matières, des indications sur chaque étape de l’encodage avec exemple à l’appui sont données. 
---
* Visite guidée des guidelines

	Où trouver ce qu’on cherche dans les guidelines ?
	Dans Front matter, vous trouverez des informations sur les guidelines en elle-même. 
	Dans Back Matter, vous trouverez des index qui répertorient les éléments et  les attributs :
		<name>, <head>, <p> ; type, when, etc. 
	Dans Text Body, vous trouverez les directives par type de texte (versifié, dialogue, table, texte manuscrit).
	Dans TEI sourcecode, vous trouverez les ressources programmateurs.

* laisser quelques minutes pour aller se promener, regarder comment ça se présente, les exemples, etc *

---

# Atelier 1 : De quoi allons-nous avoir besoin pour notre source ?
	> Commencez à réfléchir au type d'encodage qui peut être approprié à notre source (le journal du Bas Rhin) : Quelle partie des guidelines pourront nous être utiles ?
	Commencez aussi à réfléchir à la façon dont le document est structuré

---
	# Les divisions & unités textuelles
	Avant d’encoder, repérer la structure générale du texte.
	Les unités d’un texte sont appelées des divisions : 
		<div>
	Ces principaux attributs permettent de numéroter la division @n et d’indiquer la nature de la division @type “chapitres”, “tomes”, “actes”, “scènes”, “volumes”, etc.
	Astuce : représenter sous forme d’un dessin


![](media/Encodage_structure.png)


Atelier dessin :)
Piège -> on est sur un corpus morcelé, fonctionnement très différent du roman par exemple ->  
Mentionner l'option de TEI corpus ?

---
	# 1. Encodage Structurel
	> Dans body, intégrer le texte dans plusieurs div et plusieurs paragraphes selon la structure que vous avez repéré
		<body>
			<div>
				<p>texte</p>
			</div>
			<div>
				<p>texte</p>
			</div>
		</body>
	> Serait-il utile d'ajouter des attributs à l'élément div ?

---
	> Astuce : raccourci pour encadrer un mot avec une balise alt | shift | w

---
# 2 Encodage sémantique
## a. Repérer les lieux, les dates, les noms
		<placename>
		<date>
		<persname>

---
### Les noms de personnes 
		­<name> : Encode n’importe quel nom propre. Le type est indiqué par @type.
		­<persName> : Élément spécifique pour les noms de personnes.
		Ex : <persName type= "auteur" >Albert Camus</persName>
	Possibilité d'être beaucoup plus précis encore :
		­<persName> peut être compléter par d’autres éléments afin de distinguer 
		le prénom (<forename>), 
		le nom (<surname>), 
		la particule (<nameLink>), 
		le titre ou le rang (<roleName>)
		<addName> nom additionnel comme un surnom, une épithète, un alias ou toute autre expression descriptive utilisée dans un nom de personne.

---

	# Atelier 2
	## Encoder des lieux

---

# Les lieux
		<placeName> : pays, ville, village, bâtiments…
		Ex : <placeName type=“ville”>Lorient</placeName>
		<district> : subdivision d’une ville
		<settlement> : ville / village ; lieu de peuplement.
		<region> : unité administrative, état / province / région.
		<country> : pays, colonie, communauté. Unité géo-politique
		<bloc> : unité géopolitique qui rassemble plusieurs pays.

---
# Atelier 3
## Encoder des lieux

---

	# Les dates
		­<date> : Encode n’importe quelle date (implicite ou explicite).
		   Ex : <date>le 10 novembre 2020</date>
		          <date>l’année dernière</date>
		
		­<date> + @when
		   Ex : <date when="2019-06-10">le 10 juin 2019</date>
		          <date when="2018">l’année dernière</date>
		
		<date> + @from/@to
		   Ex : <date from=« 2016-01" to="2016-12">l’année dernière</date>

---
	> Besoin d'une pause ? ;)
---
## b. Créer un index des lieux
	La TEI permet de créer des listes de personnes ou de lieux auxquelles vous pouvez faire référence dans votre texte (comme un index).
		La liste se crée avec <listPlace> (dans le <teiHeader>, <profileDesc>, <settingDesc>). Chaque élément de la liste est encodé avec <place>.
		
		<listPlace>
		<place xml:id="St">
		<placeName>Strasbourg</placeName>
		<location><geo> N XXXXX E XXXXX </geo> </location>
		<idno type="URI>https://...wikidata</idno>
		<idno type="URI>https://...geonames</idno>
		<note></note>
		</place>
		</listPlace>


---
	Vous pouvez faire référence à cette liste dans votre texte avec @corresp.
		Ex : <placeName corresp=“#St”>Strasbourg</placeName>
	
	NB : Il existe des attributs globaux (essentiels en TEI).
		­@xml:id : Ajout d’un identifiant unique à un élément.
		­@ref : Référence à un identifiant unique (VIAF etc).
		­@xml:lang : Indique la langue d’un texte ou d’une portion de texte.
		­@type : Permet de créer une typologie.

---
# Balisage sémantique : les autres cas de figure
	Varie en fonction des corpus et des questions spécifiques auquel le projet de recherche souhaite répondre.
	Peut concerner les références citées, les émotions, des thèmes particuliers, des évènements, etc.

---
# Cas pratique
	Hier, j'ai lu un *Tom-Tom et Nana*.
	--------------------- ---------
	Tu as vraiment osé dire que cette présentation était *interminable* ?
	--------------------- ---------
	Cette bibliothèque numérique est vraiment *cute*.
	--------------------- ---------
	> -> Comment encoder ces phrases ?

---

	> Encodage sémantique VS encodage descriptif
	
	Différencier comment les choses *s'affichent* (ce n'est pas l'affaire de la TEI) et ce qu'elles *signifient*. 
	-> Appliquer à une signification un effet visuel, c'est ce qui se passe **après** l'encodage (dans le CSS, et cela peut varier selon les supports, sites, contraintes éditoriales).

---
# TEI Header

---

	# 4 principales catégories :
	1. fileDesc : Description du fichier XML-TEI (Balises minimales obligatoires) ;
	2. encodingDesc : Description du projet et des choix éditoriaux (corrections, particularités de l’encodage…) ;
	3. profileDesc : Description du contexte de production, de la langue, du sujet…
	4. revisionDesc : Historique des changements et des révisions du fichier TEI.
	
	(5. xenoData : Pour les métadonnées provenant de systèmes non TEI.)
---

# Roma

---
	# Visite guidée de projets numériques qui utilisent la TEI
	Voir le document dans le Git avec des liens
---
	# Ressources complémentaires 
	- Lecture : https://books.openedition.org/oep/1298?lang=fr 
	- Cours en ligne dariah : https://teach.dariah.eu/course/view.php?id=32&section=0
	- D'autres slides : https://halshs.archives-ouvertes.fr/cel-01706530/file/01_TEI_Introduction_Camps_20151120.pdf 
	- Un espace d’entraînement : https://teibyexample.org/exist/ 

---
# En guise de conclusion
# Points positifs
	Standard partagé, ouvert, libre, indépendant d'un univers logiciel
	Respect du FAIR
	Permet de dépasser les frontières disciplinaires 
	Communauté active

# Communauté -> le consortium TEI
	- Mise en place de guidelines (elles spécifient des méthodes d'encodage pour les textes lisibles par machine, principalement dans les domaines des sciences humaines, des sciences sociales et de la linguistique)
	- Centralisation de ressources (bibliographies, index, logiciels développés pour ou adaptés à la TEI).
	- Organisation d'évènements (formations, ateliers, etc.)
 
---
# Discussion :)
	Pour ceux qui le souhaitent, possibilité d'échanger sur quelques cas particuliers de l'encodage
	- Image / élément graphique
	- Tabulation / table des matières
	- Page de titre
